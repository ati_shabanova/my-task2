package az.ingress.task2.dto;

import az.ingress.task2.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {

    private Long id;
    private String name;
    private String institute;
    private LocalDate birtDate;

    public StudentDto(Student student){
        this.id = student.getId();
        this.name = student.getName() ;
        this.institute = student.getInstitute();
        this.birtDate =  student.getBirtDate();

    }
}
