package az.ingress.task2.entity;

import az.ingress.task2.dto.StudentDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @Id
    private Long id;
    private String name;
    private String institute;
    private LocalDate birtDate;


    public Student(StudentDto studentDto){
        this.id = studentDto.getId();
        this.name = studentDto.getName() ;
        this.institute = studentDto.getInstitute();
        this.birtDate =  studentDto.getBirtDate();

    }
}
