package az.ingress.task2.controller;

import az.ingress.task2.dto.StudentDto;
import az.ingress.task2.entity.Student;
import az.ingress.task2.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping
    public ResponseEntity<List<StudentDto>> getAllStudents(){
        return new ResponseEntity<> (studentService.getAllStudents(),HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDto> getStudentById(@PathVariable Long id){
        return new ResponseEntity<> (studentService.getStudentById(id),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<StudentDto> updateStudentById(@PathVariable Long id,@RequestBody StudentDto studentDto){
        return  new ResponseEntity<> (studentService.updateStudentById(id,studentDto),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity insertStudent(@RequestBody StudentDto studentDto){
        studentService.insertStudent(studentDto);
        return  new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StudentDto> deleteStudentById(@PathVariable Long id){
        studentService.deleteStudentById(id);
        return  new ResponseEntity<> (HttpStatus.OK);
    }

}
