package az.ingress.task2.service;

import az.ingress.task2.dto.StudentDto;
import az.ingress.task2.entity.Student;
import az.ingress.task2.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public List<StudentDto> getAllStudents(){
        List<StudentDto> list = new ArrayList<>();
        for (Student student:studentRepository.findAll()) {
            list.add(new StudentDto(student));
        }
        return  list;

    }

    public StudentDto getStudentById(Long id) {
        return new StudentDto(studentRepository.findById(id).get());
    }

    public StudentDto updateStudentById(Long id,StudentDto studentDto){
        studentDto.setId(id);
        return  new StudentDto(studentRepository.save (new Student(studentDto)) );
    }

    public void insertStudent(StudentDto studentDto){
        studentRepository.save(new Student(studentDto));
    }

    public void deleteStudentById(Long id) {
        studentRepository.deleteById(id);
    }
}
